/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AutoRename_free.sp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: DragonSoul <dragonsoul@ovh.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/07 13:55:15 by DragonSoul        #+#    #+#             */
/*   Updated: 2015/06/16 00:36:31 by DragonSoul       ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sourcemod>

new Handle:g_NicknameChange;
forward OnNicknameChange(client);

public Plugin:myinfo =
{
	name = "AutoRename",
	description = "Set and save your nickname.",
	author = "DragonSoul",
	version = "0.8",
	url = "http://www.cscargo.net"
};

/*
** cr�ation du forward et des commandes
*/
public OnPluginStart()
{
	g_NicknameChange = CreateGlobalForward("OnNicknameChange", ET_Event, Param_Cell);
	RegConsoleCmd("sm_name", ChangeName, "Change your nickname", 0);
	RegConsoleCmd("sm_resetname", ResetName, "Reset your nickname", 0);
	// changer tag selon besoin
	RegAdminCmd("sm_forcename", ForceName, ADMFLAG_KICK, "Change a player's nickname");
}

/*
** change son propre pseudo et l'enregistre dans la BDD
*/
public Action:ChangeName(client, args)
{
	new String:arg1[64] = "player";
	new String:name[64];

	if (args >= 1)
		GetCmdArg(1, arg1, sizeof(arg1));
	GetClientName(client, name, sizeof(name));
	ServerCommand("sm_rename %s %s", name, arg1);
	RegName(client, arg1);
	Call_nick_handler(client);
	return	Plugin_Handled;
}

/*
** force le renommage d'un joueur, et l'enregistre dans la BDD
*/
public Action:ForceName(client, args)
{
	new String:arg1[64] = "player";
	new String:name[64];
	new String:str[64];
	new p_client = 0;

	if (args >= 2)
	{
		GetCmdArg(1, name, sizeof(name));
		GetCmdArg(2, arg1, sizeof(arg1));
		if (name[0] == '#')
		{
			strcopy(str, sizeof(str), name[1]);
			p_client = StringToInt(str);
		}
		else
			p_client = FindTarget(client, name, false);
		if (p_client > 0)
		{
			ServerCommand("sm_rename %s %s", name, arg1);
			RegName(p_client, arg1);
			Call_nick_handler(p_client);
		}
		else
			PrintToConsole(client, "sm_forcename: player not found");
	}
	else
		PrintToConsole(client, "Usage: sm_forcename <#ID | act_nickname> <new_nickname>");
	return	Plugin_Handled;
}

/*
** enregistre le nouveau nom dans la BDD (ajout ou remplacement par rapport au steamid)
*/
RegName(client, const String:name[])
{
	new String:error[255];
	new Handle:db;
	new Handle:query;
	new String:steamid[64];
	new String:squery[256];
 
	GetClientAuthId(client, AuthId_Steam2, steamid, sizeof(steamid));
	Format(squery, sizeof(squery), "INSERT INTO nicknames VALUES (\'%s\', \'%s\')", steamid, name);
	db = SQL_DefConnect(error, sizeof(error));
	if (db == INVALID_HANDLE)
		PrintToServer("Could not connect to DB: %s", error);
	else
	{
		query = SQL_Query(db, squery);
		if (query == INVALID_HANDLE)
		{
			Format(squery, sizeof(squery), "UPDATE nicknames SET name = \'%s\' WHERE id LIKE \'%s\'", name, steamid);
			query = SQL_Query(db, squery);
			if (query == INVALID_HANDLE)
			{
				SQL_GetError(db, error, sizeof(error));
				PrintToServer("Failed to query (error: %s)", error);
			}
		}
		else
			CloseHandle(query);
		CloseHandle(db);
	}
}

/*
** supprime l'enregistrement de pseudo existant
** le pseudo en jeu redeviendra alors le pseudo steam
*/
public Action:ResetName(client, args)
{
	new String:error[255];
	new Handle:db;
	new Handle:query;
	new String:steamid[64];
	new String:squery[256];
 
	GetClientAuthId(client, AuthId_Steam2, steamid, sizeof(steamid));
	Format(squery, sizeof(squery), "DELETE FROM nicknames WHERE id LIKE \'%s\'", steamid);
	db = SQL_DefConnect(error, sizeof(error));
	if (db == INVALID_HANDLE)
		PrintToServer("Could not connect to DB: %s", error);
	else
	{
		query = SQL_Query(db, squery);
		if (query == INVALID_HANDLE)
		{
			SQL_GetError(db, error, sizeof(error));
			PrintToServer("Failed to query (error: %s)", error);
		}
		else
			CloseHandle(query);
		CloseHandle(db);
	}
	return	Plugin_Handled;
}

/*
** cree un forward "OnNicknameChange"
** permet la programmation evenementielle
*/
Call_nick_handler(client)
{
	new Action:result;

	Call_StartForward(g_NicknameChange);
	Call_PushCell(client);
	Call_Finish(_:result);
}

/*
** Si un pseudo est enregistré pour un joueur se connectant au serveur, le renomme automatiquement.
*/
public OnClientPutInServer(client)
{
	new String:error[255];
	new Handle:db;
	new Handle:query;
	new String:steamid[64];
	new String:squery[256];
	new String:name[64] = "";
	new String:cname[64];
 
	if(IsFakeClient(client))
		return ;
	db = SQL_DefConnect(error, sizeof(error));
	if (db == INVALID_HANDLE)
	{
		PrintToServer("Could not connect to DB: %s", error);
	}
	else
	{
		GetClientAuthId(client, AuthId_Steam2, steamid, sizeof(steamid));
		GetClientName(client, cname, sizeof(cname));
		Format(squery, sizeof(squery), "SELECT DISTINCT name FROM nicknames WHERE id LIKE \'%s\'", steamid);
		query = SQL_Query(db, squery);
		if (query == INVALID_HANDLE)
		{
			SQL_GetError(db, error, sizeof(error));
			PrintToServer("Failed to query (error: %s)", error);
		}
		else
		{
			if (SQL_FetchRow(query))
			{
				SQL_FetchString(query, 0, name, sizeof(name));
				if (!StrEqual(name, cname, true))
					ServerCommand("sm_rename %s %s", cname, name);
			}
			CloseHandle(query);
		}
		CloseHandle(db);
	}
}





