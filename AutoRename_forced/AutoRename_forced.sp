/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AutoRename_forced.sp                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: DragonSoul <dragonsoul@ovh.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/07 13:55:15 by DragonSoul        #+#    #+#             */
/*   Updated: 2015/07/26 06:22:48 by DragonSoul       ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sourcemod>

#define MAXCHARS 500
#define CHAR_MAXLEN 5
#define NAME_LEN 64

/* ************************************************************************** */
/*                                    NOTE                                    */
/*                                                                            */
/* - Un nom contenant un caract�re ap�cial apr�s un espace, sera coup� juste  */
/*   avant l'espace                                                           */
/*                                                                            */
/* ************************************************************************** */

new String:g_authorised[500] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
new g_ok_chars = 52;
new String:g_config[NAME_LEN] = "/addons/sourcemod/configs/authorized.cfg";


new Handle:g_NicknameChange;
forward OnNicknameChange(client);

public Plugin:myinfo =
{
	name = "AutoRename",
	description = "Set and save your nickname.",
	author = "DragonSoul",
	version = "0.9",
	url = "http://www.cscargo.net"
};

/*
** cr�ation du forward et des commandes
*/
public OnPluginStart()
{
	g_NicknameChange = CreateGlobalForward("OnNicknameChange", ET_Event, Param_Cell);
	RegConsoleCmd("sm_name", ChangeName, "Change your nickname", 0);
	// changer tag selon besoin
	RegAdminCmd("sm_forcename", ForceName, ADMFLAG_KICK, "Change a player's nickname");
	RegAdminCmd("sm_reloadchar", ReloadConfig, ADMFLAG_CONFIG, "Recharger le fichier de config des caractere autoris�s");
}

public OnMapStart()
{
	InitiateConfig();
	
}

public Action:ReloadConfig(client, args) {
	
	InitiateConfig();
	LogAction(client, -1, "AutoRename: Reloaded config file");
	ReplyToCommand(client, "AutoRename: Reloaded config file");
	return Plugin_Handled;
}

/*
** Initialise la configuration du filtre de caract�res
*/
stock InitiateConfig()
{
	new i = -1;
	new Handle:authorised_char = OpenFile(g_config, "r");
	if (authorised_char != INVALID_HANDLE)
	{
		while ((++i < MAXCHARS) && ReadFileLine(authorised_char, g_authorised[i], CHAR_MAXLEN))
			TrimString(g_authorised[i]);
		PrintToServer("AutoRename: Nombre de caract�res authoris�s : %d ",i); // compte aussi les lignes vides
		g_ok_chars = i;
		CloseHandle(authorised_char);
	}
}

/*
** V�rifie que le caract�re soit autoris�.
*/
is_correct_char(String:c)
{
	for(new i = 0; i < g_ok_chars; ++i)
	{
		if (g_authorised[i] == c) // attention, 5 caracteres pour un special ??
			return (true);
	}
	return (false);
}

/*
** Renvoit le caract�re de remplacement
*/
get_correct_char(String:c)
{
	return ('.');
}

/*
** modifie le pseudo selon les r�gles
*/
correct_nickname(String:nickname[NAME_LEN])
{
	new i = -1;
	while (nickname[++i] && i < NAME_LEN - 1)
	{
		if (!is_correct_char(nickname[i]))
			nickname[i] = get_correct_char(nickname[i]); // ReplaceString ???
	}
	nickname[i] = '\0';
}

/*
** change son propre pseudo et l'enregistre dans la BDD
*/
public Action:ChangeName(client, args)
{
	new String:arg1[NAME_LEN] = "player";
	new String:name[NAME_LEN];

	if (args >= 1)
		GetCmdArg(1, arg1, sizeof(arg1));
	GetClientName(client, name, sizeof(name));
	correct_nickname(arg1);
	ServerCommand("sm_rename \"%s\" \"%s\"", name, arg1);
	RegName(client, arg1);
	Call_nick_handler(client);
	return	Plugin_Handled;
}

/*
** force le renommage d'un joueur, et l'enregistre dans la BDD
*/
public Action:ForceName(client, args)
{
	new String:arg1[NAME_LEN] = "player";
	new String:name[NAME_LEN];
	new String:str[NAME_LEN];
	new p_client = 0;

	if (args >= 2)
	{
		GetCmdArg(1, name, sizeof(name));
		GetCmdArg(2, arg1, sizeof(arg1));
		if (name[0] == '#')
		{
			strcopy(str, sizeof(str), name[1]);
			p_client = StringToInt(str);
		}
		else
			p_client = FindTarget(client, name, false);
		if (p_client > 0)
		{
			correct_nickname(arg1);
			ServerCommand("sm_rename \"%s\" \"%s\"", name, arg1);
			RegName(p_client, arg1);
			Call_nick_handler(p_client);
		}
		else
			PrintToConsole(client, "sm_forcename: player not found");
	}
	else
		PrintToConsole(client, "Usage: sm_forcename <#ID | act_nickname> <new_nickname>");
	return	Plugin_Handled;
}

/*
** enregistre le nouveau nom dans la BDD (ajout ou remplacement par rapport au steamid)
*/
RegName(client, const String:name[])
{
	new String:error[255];
	new Handle:db;
	new Handle:query;
	new String:steamid[NAME_LEN];
	new String:squery[256];
 
	GetClientAuthId(client, AuthId_Steam2, steamid, sizeof(steamid));
	Format(squery, sizeof(squery), "INSERT INTO nicknames VALUES (\'%s\', \'%s\')", steamid, name);
	db = SQL_DefConnect(error, sizeof(error));
	if (db == INVALID_HANDLE)
		PrintToServer("Could not connect to DB: %s", error);
	else
	{
		query = SQL_Query(db, squery);
		if (query == INVALID_HANDLE)
		{
			Format(squery, sizeof(squery), "UPDATE nicknames SET name = \'%s\' WHERE id LIKE \'%s\'", name, steamid);
			query = SQL_Query(db, squery);
			if (query == INVALID_HANDLE)
			{
				SQL_GetError(db, error, sizeof(error));
				PrintToServer("Failed to query (error: %s)", error);
			}
		}
		else
			CloseHandle(query);
		CloseHandle(db);
	}
}

/*
** cree un forward "OnNicknameChange"
** permet la programmation evenementielle
*/
Call_nick_handler(client)
{
	new Action:result;

	Call_StartForward(g_NicknameChange);
	Call_PushCell(client);
	Call_Finish(_:result);
}

/*
** Si un pseudo est enregistre pour un joueur se connectant au serveur, le renomme automatiquement.
*/
public OnClientPutInServer(client)
{
	new String:error[255];
	new Handle:db;
	new Handle:query;
	new String:steamid[NAME_LEN];
	new String:squery[255];
	new String:name[NAME_LEN] = "";
	new String:cname[NAME_LEN];
 
	if(IsFakeClient(client))
		return ;
	db = SQL_DefConnect(error, sizeof(error));
	if (db == INVALID_HANDLE)
	{
		PrintToServer("Could not connect to DB: %s", error);
	}
	else
	{
		GetClientAuthId(client, AuthId_Steam2, steamid, sizeof(steamid));
		GetClientName(client, name, sizeof(cname));
		GetClientName(client, cname, sizeof(cname));
		Format(squery, sizeof(squery), "SELECT DISTINCT name FROM nicknames WHERE id LIKE \'%s\'", steamid);
		query = SQL_Query(db, squery);
		if (query == INVALID_HANDLE)
		{
			SQL_GetError(db, error, sizeof(error));
			PrintToServer("Failed to query (error: %s)", error);
		}
		else
		{
			if (SQL_FetchRow(query))
			{
				SQL_FetchString(query, 0, name, sizeof(name));
				if (!StrEqual(name, cname, true))
					ServerCommand("sm_rename \"%s\" \"%s\"", cname, name);
			}
			else
			{
				correct_nickname(name);
				ServerCommand("sm_rename \"%s\" \"%s\"", cname, name);
				RegName(client, name);
			}
			CloseHandle(query);
		}
		CloseHandle(db);
	}
}

